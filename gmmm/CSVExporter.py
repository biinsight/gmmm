import csv
import io
from typing import List, Tuple, Optional

from json_normalize import json_normalize


def export_to_csv(result: List[dict], encoding: str = 'cp1250', errors: str = "ignore",
                  normalize: Optional[bool] = False, delimiter: Optional[str] = ',', custom_sort: Optional[bool] = False, sort_list: Optional[List[str]] = None) -> bytes:
    result, headers = prepare_to_csv(result, custom_sort=custom_sort, sort_list=sort_list)
    with io.StringIO() as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=headers, extrasaction='ignore', delimiter=delimiter)
        writer.writeheader()
        for row in result:
            processed_row = {key: ";".join(value) if isinstance(value, list) else value for key, value in row.items()}
            writer.writerow(processed_row)
        csv_bytes = csv_file.getvalue().encode(encoding=encoding, errors=errors)
    return csv_bytes


def prepare_to_csv(result: List[dict], normalize: Optional[bool] = False, custom_sort: Optional[bool] = False, sort_list: Optional[List[str]] = None) -> Tuple[List[dict], List[str]]:
    if normalize:
        result = list(json_normalize(result))
    headers = []
    for i in result:
        headers.extend(list(i.keys()))
    headers = list(set(headers))
    headers.sort()
    if custom_sort and sort_list is not None:
        headers.sort(key=lambda x: sort_list.index(x) if x in sort_list else float('inf'))
    return result, headers
