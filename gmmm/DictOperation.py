from gmmm import StringOperation


def camel_to_snake_case(dictionary: dict) -> dict:
    """Change camel case keys to snake case keys

    Args:
        dictionary: dict with camel case keys

    Returns:

    """
    snake_names_dict = dict()
    for field_name in dictionary:
        snake_names_dict[StringOperation.camel_to_snake_case(field_name)] = dictionary[field_name]
    return snake_names_dict


def snake_to_camel_case(dictionary: dict) -> dict:
    """Change snake case keys to camel case keys

    Args:
        dictionary: dict with snake case keys

    Returns:

    """
    camel_case_dict = dict()
    for field_name in dictionary:
        camel_case_dict[StringOperation.snake_to_camel_case(field_name)] = dictionary[field_name]
    return camel_case_dict


def change_key_name(dictionary: dict, old_key: str, new_key: str) -> dict:
    """ Change key name in dictionary

    Args:
        dictionary: dictionary to change
        old_key: key name to change
        new_key: new key name

    Returns:
        dict

    """
    dictionary[new_key] = dictionary[old_key]
    del dictionary[old_key]
    return dictionary


def change_names_to_camelcase(dictionary: dict):
    """Change case of key and value of dictionary to camel case

    Args:
        dictionary (dict):

    Returns:
        dict
    """
    new_dict_object = dict()
    if type(dictionary) == dict:
        for attribute_name in dictionary.keys():
            if type(dictionary[attribute_name]) == dict:
                new_dict_object[StringOperation.snake_to_camel_case(attribute_name)] \
                    = change_names_to_camelcase(dictionary[attribute_name])
                continue
            if type(dictionary[attribute_name]) == list:
                object_list = dictionary[attribute_name]
                temp_list = list()
                for element in object_list:
                    if type(element) == dict:
                        new_nested_object = change_names_to_camelcase(element)
                        temp_list.append(new_nested_object)
                    else:
                        temp_list.append(element)
                new_dict_object[StringOperation.snake_to_camel_case(attribute_name)] = temp_list
                continue
            new_dict_object[StringOperation.snake_to_camel_case(attribute_name)] = dictionary[attribute_name]
    return new_dict_object


def change_names_to_snake_case(dictionary: dict):
    """Change case of key and value of dictionary to snake case

    Args:
        dictionary (dict):

    Returns:
        dict
    """
    new_dict_object = dict()
    if type(dictionary) == dict:
        for attribute_name in dictionary.keys():
            if type(dictionary[attribute_name]) == dict:
                new_dict_object[StringOperation.camel_to_snake_case(attribute_name)] \
                    = change_names_to_snake_case(dictionary[attribute_name])
                continue
            if type(dictionary[attribute_name]) == list:
                object_list = dictionary[attribute_name]
                temp_list = list()
                for element in object_list:
                    if type(element) == dict:
                        new_nested_object = change_names_to_snake_case(element)
                        temp_list.append(new_nested_object)
                    else:
                        temp_list.append(element)
                new_dict_object[StringOperation.camel_to_snake_case(attribute_name)] = temp_list
                continue
            new_dict_object[StringOperation.camel_to_snake_case(attribute_name)] = dictionary[attribute_name]
    return new_dict_object


def get_dict_without_none_values(**filters) -> dict:
    """Removes from dict keys with empty values

    Args:
        **filters:
    Returns:
        dict
    """
    get_dict = {k: v for k, v in filters.items() if v is not None}
    return get_dict
