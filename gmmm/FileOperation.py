import os
from datetime import datetime


def get_file_bytes(path: str) -> bytes:
    """Read and return bytes of file

    Args:
        path: path to file

    Returns:
        bytes

    """

    with open(path, 'rb') as f:
        file_bytes = f.read()
    return file_bytes


def get_directory_separator(path: str) -> str:
    """Return directory separator from path

    Return "\" or "/"

    Args:
        path: file path

    Returns:
        str

    """
    backslash_pos = path.find('\\')
    slash_pos = path.find('/')
    if backslash_pos == -1 and slash_pos == -1:
        raise RuntimeError("Cannot resolve directory separator from path: {}".format(path))
    elif backslash_pos == -1:
        return '/'
    elif slash_pos == -1:
        return '\\'
    elif slash_pos < backslash_pos:
        return '/'
    else:
        return '\\'


def get_extension_from_file(path: str) -> str: 
    """Return extension of file

    Return extension of file from directory path.
    Method splits directory (using ".") and takes last second element of list.

    Args:
        path: file path

    Returns:
        str

    """
    _, extension = os.path.splitext(path)
    extension = extension.strip('.')
    str(extension).lower()
    return extension


def get_file_size(path: str) -> int:
    """Return size of file

    Check and return size of file using os module.
    Args:
        path: file path

    Returns:
        int

    """
    return os.path.getsize(path)


def get_file_creation_date(path: str):
    """Return creation date of file

    Using os module and datetime module

    Args:
        path: file path

    Returns:
        datetime

    """
    return datetime.fromtimestamp(os.path.getctime(path)).astimezone()


def get_file_last_modified(path: str):
    """Return last modification date of file

    Using os module and datetime module

    Args:
        path: file path

    Returns:
        datetime

    """
    return datetime.fromtimestamp(os.path.getmtime(path)).astimezone()
