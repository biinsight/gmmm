import re


def remove_empty_elements(list_of_elements: list) -> list:
    """Remove empty elements from list

    Args:
        list_of_elements: list with empty elements

    Returns:
        list

    """
    return [x for x in list_of_elements if x]


def remove_whitespace_elements(list_of_elements) -> list:
    """Remove whitespace elements from list

    Args:
        list_of_elements: list with whitespace elements

    Returns:
        list

    """
    pattern = re.compile(r'\s+')
    return [x for x in list_of_elements if not re.search(pattern=pattern, string=x)]

