from typing import Union, List
from collections.abc import Iterable

from gmmm.DictOperation import change_names_to_camelcase


def make_dict_from_object(obj: object) -> dict:
    """Transform object to dict

    Args:
        obj: object to transform

    Returns:
        dict
    """
    new_dict_object = obj
    iter_array = obj.__annotations__ if hasattr(obj, '__annotations__') else vars(obj)
    for attribute_name in iter_array:
        if not hasattr(obj, attribute_name):
            continue
        if hasattr(getattr(obj, attribute_name), "__dict__"):
            if isinstance(getattr(obj, attribute_name), Iterable):
                setattr(new_dict_object, attribute_name, getattr(obj, attribute_name))
            else:
                setattr(new_dict_object, attribute_name, vars(getattr(obj, attribute_name)))
            continue
        if type(getattr(obj, attribute_name)) == list:
            list_of_objects = getattr(obj, attribute_name)
            if len(getattr(obj, attribute_name)) > 0:
                temp_list = make_dict_from_list_object(list_of_objects)
                setattr(new_dict_object, attribute_name, temp_list)
            continue
        setattr(new_dict_object, attribute_name, getattr(obj, attribute_name))
    return vars(new_dict_object)


def make_dict_from_list_object(list_of_objects: list) -> list:
    """Transform list of objects to dict

    Args:
        list_of_objects: list of objects to transform

    Returns:

    """
    temp_list = list()
    if hasattr(list_of_objects[0], "__dict__"):
        for element in list_of_objects:
            temp_list.append(make_dict_from_object(element))
    else:
        for element in list_of_objects:
            temp_list.append(element)
    return temp_list


def _prepare_json(obj: object) -> dict:
    """
    HAS BEEN EXTENDED BY PROPER PREPARE_JSON METHOD
    Create dict from object

    Method transform api object to camel case dict that can be send to GUI.

    Args:
        obj: api object

    Returns:
        dict

    """
    dict_object = make_dict_from_object(obj)
    camelcase_dict = change_names_to_camelcase(dict_object)
    return camelcase_dict


def prepare_json(obj: object) -> Union[dict, List[dict]]:
    """
    Transforms an object into dict - can return list of dicts when provided with list.
    Args:
        obj: Any object

    Returns:
        Python dict or list of dicts.

    Authors:
        Szymon Nowak
    """
    if isinstance(obj, dict):
        return change_names_to_camelcase(obj)
    if isinstance(obj, list):
        json_list = []
        for elem in obj:
            json_list.append(prepare_json(elem))
        return json_list
    else:
        return _prepare_json(obj)
