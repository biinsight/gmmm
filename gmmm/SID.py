import struct


class SID(object):

    def __init__(self):
        self.fmt = {True: '<q', False: '>q'}
        self.ret = 'S'
        self.size = 4
        self.width = 8
        self.fill_char = b'\x00'
        self.separator = '-'

    def _le_long_to_bytes(self, integer: int, size: int):
        return struct.pack(self.fmt[True], integer)[0:size]

    def _be_long_to_bytes(self, integer: int, size: int):
        return struct.pack(self.fmt[False], integer)[self.width - size:]

    def _le_bytes_to_long(self, binary: bytes):
        if len(binary) > 8:
            raise Exception('Bytes too long.')
        return struct.unpack(self.fmt[True], binary.ljust(self.width, self.fill_char))[0]

    def _be_bytes_to_long(self, binary: bytes):
        if len(binary) > 8:
            raise Exception('Bytes too long.')
        return struct.unpack(self.fmt[False], binary.rjust(self.width, self.fill_char))[0]

    def binary_to_str_sid(self, binary: bytes) -> str:
        ''' Convert bytes into a str sid '''
        str_sid = self.ret
        sid = []
        sid.append(self._le_bytes_to_long(binary=binary[0:1]))
        sid.append(self._be_bytes_to_long(binary=binary[2:2 + 6]))
        for i in range(self.width, len(binary), self.size):
            sid.append(self._le_bytes_to_long(binary=binary[i:i + self.size]))
        for i in sid:
            str_sid += self.separator + str(i)
        return str_sid

    def str_into_binary_sid(self, str_sid: str) -> bytes:
        '''Convert str sid into bytes'''
        sid = str.split(str_sid, self.separator)
        binary_sid = bytearray()
        sid.remove(self.ret)
        for i in range(len(sid)):
            sid[i] = int(sid[i])
        sid.insert(1, len(sid) - 2)
        binary_sid += self._le_long_to_bytes(integer=sid[0], size=1)
        binary_sid += self._le_long_to_bytes(integer=sid[1], size=1)
        binary_sid += self._be_long_to_bytes(integer=sid[2], size=6)
        for i in range(3, len(sid)):
            binary_sid += self._le_long_to_bytes(integer=sid[i], size=self.size)
        return bytes(binary_sid)
