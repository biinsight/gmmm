import re
import string

from gmmm import values


def camel_to_snake_case(text: str) -> str:
    """Change camel case text to snake case

    Method uses regular expression.

    Args:
        text(str): text to change to snake case

    Returns:
        str: snake case text

    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', text)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def snake_to_camel_case(text: str) -> str:
    """Change snake case text to camel case

    Method uses regular expression.

    Args:
        text(str): text to change to camel case

    Returns:
        str: camel case text

    """
    return re.sub(r'(?!^)_([a-zA-Z])', lambda m: m.group(1).upper(), text)


def remove_punctuation(text: str) -> str:
    """Remove punctuation

    Method decodes text in utf8 and removes punctuation.
    Removed symbols: !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~■☒☐�][“”

    Args:
        text(str): text with punctuation

    Returns:
        str: text without punctuation

    """
    if isinstance(text, str):
        punct = string.punctuation + '■☒☐�][“”'
        text = text.encode('utf8').decode('utf8')
        text = str.lower(text)
        text = text.translate(str.maketrans('', '', punct))
    else:
        text = ''
    return text


def remove_numbers(text: str) -> str:
    """Remove numbers

    Method removes all of the numbers in text.
    Method uses regular expression.

    Args:
        text(str): text with numbers

    Returns:
        str: text without numbers

    """
    if isinstance(text, str):
        text = re.sub(r'[0-9]+', '', text)
    else:
        text = ""
    return text


def remove_redundant_whitespace(text: str) -> str:
    """Remove redundant whitespaces

    Method replaces all redundant whitespaces into space.
    Method uses regular expression.

    Args:
        text (str): text to clean

    Returns:
        str: text without redundant whitespaces

    """
    if isinstance(text, str):
        text = re.sub(r'\s+', ' ', text).strip()
    else:
        text = ""
    return text


def remove_hyphenate(text: str) -> str:
    """Remove hyphenate

    Method uses regular expression.

    Args:
        text: text to process

    Returns:
        str: text without hyphenate

    """
    return re.sub(r'-[\n]', '', text)


def remove_stop_words(text: str, stopwords=None, language=None) -> str:
    """Remove stopwords

    Method removes words from stopwords.
    Only one of parameters: stopwords, language has to be filled.
    Method uses custom list of stopwords or one of built-in  list.
    Built-in list has polish and english stopwords.

    Args:
        text(str): text to clean
        stopwords(list): custom list of stopwords
        language(str): stopwords language (pl/en)

    Returns:
        str: text without stopwords

    Raises:
        ValueError
        TypeError

    """

    if language is None and stopwords is None:
        raise TypeError
    if stopwords:
        stop_words_list = list(map(str.lower, stopwords))
    elif language == "pl":
        stop_words_list = values.PL_STOPWORDS
    elif language == "en":
        stop_words_list = values.EN_STOPWORDS
    else:
        raise ValueError
    text = text.split()
    text_without_stopwords = []
    for t in text:
        if t.lower() not in stop_words_list:
            text_without_stopwords.append(t)
    return " ".join(text_without_stopwords)

