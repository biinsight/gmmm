import re
import string


class DocumentCleaner:
    """Remove punctuation, numbers and redundant whitespaces from text."""

    @staticmethod
    def remove_punctuation(text: str) -> str:
        """Remove text punctuation.

        Method decodes text in utf8 and removes punctuation.
        Removed symbols: !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~■

        Args:
            text (str): text to clean

        Returns:
            str: text without punctuation

        """
        if not isinstance(text, str):
            raise ValueError

        punct = string.punctuation + '■☒☐�][“”'
        return text.lower().translate(str.maketrans('', '', punct))

    @staticmethod
    def remove_numbers(text: str) -> str:
        """Remove numbers in text.

        Method removes all of the numbers in text.
        Method uses regular expression.

        Args:
            text (str): text to clean

        Returns:
            str: text without numbers
        """
        if not isinstance(text, str):
            raise ValueError

        return re.sub(r'[0-9]+', '', text)

    @staticmethod
    def remove_redundant_whitespace(text: str) -> str:
        """Remove redundant whitespaces

        Method replaces all redundant whitespaces into space.
        Method uses regular expression.

        Args:
            text (str): text to clean

        Returns:
            str: text without redundant whitespaces

        """
        if not isinstance(text, str):
            raise ValueError

        return re.sub(r'\s+', ' ', text).strip()

    @staticmethod
    def remove_hyphenate(text: str) -> str:
        text = re.sub(r'-[\n]', '', text)
        return text

    @staticmethod
    def clean_text(text: str) -> str:
        """Remove punctuation, numbers and whitespaces from text

        Method combines methods: remove_redundant_whitespaces, remove_punctuation and remove_numbers.

        Args:
            text(str): text to clean

        Returns:
            str: text without punctuation, numbers and redundant whitespaces (utf8 encoding)
        """
        if not isinstance(text, str):
            raise ValueError

        text = DocumentCleaner.remove_numbers(text)
        text = DocumentCleaner.remove_punctuation(text)
        return DocumentCleaner.remove_redundant_whitespace(text)
