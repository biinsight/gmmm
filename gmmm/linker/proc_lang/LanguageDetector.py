from langdetect import DetectorFactory
from langdetect import detect_langs
from langdetect.lang_detect_exception import LangDetectException


class LanguageDetector:
    """Detect language of file

    Class ensure consistent results for language detection. Class has method to detect probability
    of occurrences of languages.

    """

    def __init__(self):
        """ Enforce consistent results of language detection """
        DetectorFactory.seed = 0

    @staticmethod
    def get_most_likely_lang(plain_text: str) -> str:
        """Detect languages of text

        Method detect languages and probability of occurrence in file.

        Args:
            plain_text (str): text to language detection
        Returns:
            str: string language (ISO 639-1 codes)
        """
        try:
            detected_languages = detect_langs(plain_text)
            language = str(detected_languages[0]).split(":")
            language = language[0]
            if language:
                return language
            return "pl"
        except LangDetectException:
            return "pl"
        except TypeError:
            print("Wrong type")
            return 'pl'
