from typing import List, Dict, Union

from gmmm.linker.proc_cleaner.DocumentCleaner import DocumentCleaner
from gmmm.linker.proc_lang.LanguageDetector import LanguageDetector


def _to_plaintext(structure: List[Dict[str, Union[int, str]]]) -> str:
    struct = sorted(structure, key=lambda x: x['page_number'])
    return '\n'.join([d.get('content') for d in struct])


def to_plaintext(structure: List[Dict[str, Union[int, str]]]) -> str:
    merged = _to_plaintext(structure)
    return DocumentCleaner.remove_hyphenate(merged)


def detect_language(plaintext: str) -> str:
    return LanguageDetector.get_most_likely_lang(plaintext)
