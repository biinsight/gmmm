import json

import pika
from loguru import logger
from pika.exceptions import ChannelWrongStateError, StreamLostError, AMQPConnectionError


class RabbitMQLoggerSink:

    def __init__(self, service_name: str, username: str, password: str, host: str, port: int, queue_name: str,
                 exchange_name: str) -> None:

        self._logger = logger.bind(type='logger_info')

        self.service_name = service_name

        self.credentials = pika.PlainCredentials(username, password)
        self.connection_parameters = pika.ConnectionParameters(host, port,
                                                               credentials=self.credentials,
                                                               blocked_connection_timeout=1,
                                                               heartbeat=60)
        self.connection = None
        self.channel = None

        self.queue_name = queue_name
        self.exchange_name = exchange_name

        self.connect()

    def declare_infrastructure(self):
        self.channel.queue_declare(queue=self.queue_name)
        self.channel.exchange_declare(exchange=self.exchange_name, exchange_type='topic')
        self.channel.queue_bind(exchange=self.exchange_name, queue=self.queue_name, routing_key='#')

    def configure_connection(self):
        self.connection = pika.BlockingConnection(self.connection_parameters)

    def configure_channel(self):
        self.channel = self.connection.channel()

    def connect(self):
        try:
            self.configure_connection()
            self.configure_channel()
            self.declare_infrastructure()
        except AMQPConnectionError as e:
            self._logger.error(f"RS - cannot connect to rabbit {e}")
        except AttributeError as e:
            self._logger.error(f"RS - Channel created incorrectly {e}")

    def publish_message(self, body: str):

        # Prepare properties
        properties = pika.BasicProperties(delivery_mode=2,
                                          type='log_message',
                                          content_type='application/json',
                                          content_encoding='utf8',
                                          message_id='test_id')

        # Try send message - three times
        for _ in range(3):
            try:
                self.channel.basic_publish(self.exchange_name,
                                           '#',
                                           body,
                                           properties)
                return
            except (StreamLostError, ChannelWrongStateError, AttributeError) as e:
                self._logger.error(f'RS -  Connection lost: {e}')
                self.connect()

    def write(self, message):
        rec = message.record
        log_dict = {'time': rec['time'].isoformat(),
                    'level': rec['level'],
                    'name': rec['name'],
                    'function': rec['function'],
                    'line': rec['line'],
                    'message': rec['message'],
                    'resource_id': rec['extra'].get('resource_id'),
                    'service': self.service_name}

        rabbit_body = json.dumps(log_dict)
        self.publish_message(rabbit_body)
