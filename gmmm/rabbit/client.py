import time
from typing import Tuple, Optional

import pika
from loguru import logger
from pika import BasicProperties
from pika.adapters.blocking_connection import BlockingChannel, BlockingConnection
from pika.exceptions import StreamLostError, ChannelWrongStateError, ConnectionWrongStateError
from pika.spec import Basic


def get_exchange_and_rk(properties: pika.BasicProperties, default_exchange: str, default_rk: str) -> Tuple[str, str]:
    if properties.reply_to:
        exchange = ''
        routing_key = properties.reply_to
    else:
        exchange = default_exchange
        routing_key = default_rk

    return exchange, routing_key


def create_default_properties(old_properties: pika.BasicProperties,
                              properties_type: Optional[str] = None) -> pika.BasicProperties:
    return pika.BasicProperties(delivery_mode=2,
                                type=properties_type or old_properties.type,
                                content_type='application/python-pickle',
                                content_encoding='pickle',
                                correlation_id=old_properties.correlation_id,
                                message_id=old_properties.message_id)


class RabbitClient:
    def __init__(self, username: str, password: str, host: str, port: int,
                 blocked_connection_timeout: int = 600, heartbeat: int = 600) -> None:

        credentials = pika.PlainCredentials(username, password)
        self.connection_parameters = pika.ConnectionParameters(host=host, port=port, credentials=credentials,
                                                               blocked_connection_timeout=blocked_connection_timeout,
                                                               heartbeat=heartbeat)

        self.connection: Optional[BlockingConnection] = None
        self.channel: Optional[BlockingChannel] = None

    def callback(self, ch: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body: bytes):
        """
        This method should be overridden in derived class.
        """
        raise NotImplementedError

    def _create_connection(self) -> Tuple[BlockingConnection, BlockingChannel]:
        connection = pika.BlockingConnection(self.connection_parameters)
        return connection, connection.channel()

    def _connect(self) -> None:
        # Try to terminate old connection before creating new one.
        if self.connection:
            try:
                self.connection.close()
            except (ConnectionWrongStateError, StreamLostError) as e:
                logger.info(f'Connection already terminated. "{e}"')

        # Create new connection
        connection, channel = self._create_connection()
        self.connection = connection
        self.channel = channel

    def _subscribe_exchange(self, exchange: str, queue: str, routing_key: str) -> bool:
        if not self.channel:
            logger.info("Not connected. Call _connect() before.")
            return False
        self.channel.queue_bind(exchange=exchange, queue=queue, routing_key=routing_key)
        return True

    def create_exchange(self, exchange: str, exchange_type: str = "topic", durable: bool = True) -> bool:
        """Create new exchange
        exchange: str - name of exchange
        """
        if not self.channel:
            logger.info("Cannot create exchange")
            return False
        self.channel.exchange_declare(exchange=exchange, exchange_type=exchange_type, durable=durable)
        return True

    def _prepare_for_listening(self, queue: str, passive: bool, prefetch_count: int, auto_ack: bool,
                               bind_queue: bool = False, exchange: str = '', routing_key: str = '',
                               queue_durable: bool = True, queue_params: Optional[dict] = None) -> None:
        self._connect()
        self.channel.queue_declare(queue, passive, durable=queue_durable, arguments=queue_params)
        self.channel.basic_qos(prefetch_count=prefetch_count)
        self.channel.basic_consume(queue, self.callback, auto_ack)
        if bind_queue:
            self._subscribe_exchange(exchange, queue, routing_key)

    def start_consuming(self, queue: str, passive: bool = True, prefetch_count: int = 1,
                        auto_ack: bool = False, bind_queue: bool = False, exchange: str = '',
                        routing_key: str = '', queue_durable: bool = True, queue_params: Optional[dict] = None) -> None:
        self._prepare_for_listening(queue, passive, prefetch_count, auto_ack, bind_queue, exchange, routing_key, queue_durable,
                                    queue_params)

        while True:
            try:
                logger.info(f'Connected correctly. Consuming messages from {queue} queue')
                self.channel.start_consuming()
            except KeyboardInterrupt as e:
                logger.info(f'KeyboardInterrupt "{e}". Exiting')
                self.channel.stop_consuming()
                self.connection.close()
                return
            except Exception as e:
                logger.error(f'RabbitMQ - Exception in a callback function. "{type(e).__name__}: {e.args}".')
                time.sleep(5)
                logger.error(f'RabbitMQ - Create new connection and start consuming again.')
                self._prepare_for_listening(queue, passive, prefetch_count, auto_ack, bind_queue, exchange, routing_key)

    def connect_and_send(self, exchange: str, routing_key: str, body: bytes, properties: pika.BasicProperties) -> None:
        """Create new connection and send message."""
        connection, channel = self._create_connection()
        channel.basic_publish(exchange, routing_key, body, properties)
        try:
            connection.close()
        except StreamLostError as e:
            logger.info(f'Connection already terminated. "{e}"')

    def send_message(self, exchange: str, routing_key: str, body: bytes, properties: pika.BasicProperties) -> None:
        """Try to send message using existing connection. Create new one if connection does not exists or is closed."""
        try:
            # Try to publish message using already created channel (if exists)
            self.channel.basic_publish(exchange, routing_key, body, properties)
            logger.info('Sent correctly.')
        except (StreamLostError, ChannelWrongStateError, AttributeError) as e:
            logger.error(f'Connection is terminated or does not exists: {e}. Create new connection and send message.')
            try:
                # Create new connection and send message.
                self.connect_and_send(exchange, routing_key, body, properties)
                logger.info('Sent correctly')
            except (StreamLostError, ChannelWrongStateError, AttributeError) as e:
                logger.error(f'Connection lost: {e}. Message not sent.')
