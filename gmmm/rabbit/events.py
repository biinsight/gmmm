from enum import Enum


class EventMessageType(Enum):
    REPOSITORY_CHANGE = 'repository-change'
    PROCESSING_ERROR = 'processing-error'
    PROCESSING_SUCCESS = 'processing-success'
    TAXONOMY_CLASSIFIER = 'taxonomy-classifier'
    TAXONOMY_NODE_CLASSIFIER = 'taxonomy-node-classifier'
    NEW_DOCUMENT_ASSIGNMENT = 'new-document-assignment'
