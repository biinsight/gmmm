import json
import pickle
from enum import Enum
from typing import Tuple, Optional

import pika

from gmmm.rabbit.events import EventMessageType


class MessageContentType(Enum):
    JSON = 'application/json'
    PICKLE = 'application/python-pickle'


def decode_rabbit_message(body: bytes, properties: pika.BasicProperties) -> dict:
    """
    Decode the rabbit message body using the parameters passed in message properties.

    Args:
        body: Message body
        properties: Message properties

    Returns:
        Decoded body
    Raises:
        ValueError: Body cannot be decoded. Not enough or incorrect information has been passed in properties.
    """
    __content_data = {'content_type': properties.content_type, 'content_encoding': properties.content_encoding}

    if not properties.content_type:
        raise ValueError(f'content_type not set. {__content_data}')

    if properties.content_type == MessageContentType.JSON.value:
        if not properties.content_encoding:
            raise ValueError(f'content_encoding not set. {__content_data}')

        return json.loads(body.decode(properties.content_encoding))
    elif properties.content_type == MessageContentType.PICKLE.value:
        return pickle.loads(body)
    raise ValueError(f'Not recognized content_type. {__content_data}')


def prepare_error_message(document_id: str, service: str, page_number: Optional[int] = None,
                          encoding: str = 'utf-8') -> Tuple[bytes, pika.BasicProperties]:
    msg = {
        'document_id': document_id,
        'page_number': page_number,
        'service': service,
        'type': 'processing-error'
    }
    body = json.dumps(msg).encode(encoding)

    properties = pika.BasicProperties(
        delivery_mode=pika.DeliveryMode.Persistent,
        message_id=document_id,
        type=EventMessageType.PROCESSING_ERROR.value,
        content_type=MessageContentType.JSON.value,
        content_encoding=encoding
    )

    return body, properties


def prepare_second_try_message(original_message: dict, encoding: str = 'utf-8') -> bytes:
    original_message['second_try'] = True
    body = json.dumps(original_message).encode(encoding)
    return body


def prepare_success_message(document_id: str, service: str, routing_key: str, encoding: str = 'utf-8') -> Tuple[
                            bytes, pika.BasicProperties]:
    msg = {
        'document_id': document_id,
        'service': service,
        'type': 'processing-success',
        'routing_key': routing_key

    }
    body = json.dumps(msg).encode(encoding)

    properties = pika.BasicProperties(
        delivery_mode=pika.DeliveryMode.Persistent,
        message_id=document_id,
        type=EventMessageType.PROCESSING_SUCCESS.value,
        content_type=MessageContentType.JSON.value,
        content_encoding=encoding
    )

    return body, properties
