def validate_dict(to_validate: dict, template: dict):
    """Check if dict has fields specified in template and types of this fields are correct."""
    for field_name, expected_type in template.items():
        try:
            actual_value = to_validate[field_name]
        except KeyError:
            raise KeyError(f'Field {field_name} not found')
        if not isinstance(actual_value, expected_type):
            raise TypeError(f'Bad type. Field: {field_name}. Required: {expected_type}. Actual: {type(actual_value)}')
