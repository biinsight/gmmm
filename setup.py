from setuptools import setup, find_packages

setup(
    name='gmmm',
    version='0.0.23',
    description='Operation on String, Dict, List, Object and File',
    license='MIT',
    packages=find_packages(),
    install_requires=['loguru',
                      'pika',
                      'langdetect',
                      ],
    author=['Małgorzata Bielińska', 'Mateusz Romaniuk', 'Grzegorz Niemiec', 'Piotr Sawicki'],
    author_email=['mbielinska@biinsight.pl', 'mromaniuk@biinsight.pl', 'gniemiec@biinsight.pl',
                  'psawicki@biinsight.pl'],
    keywords=['string', 'dict', 'object', 'list', 'file', 'rabbit', 'sid'],
    url='https://bitbucket.org/biinsight/gmmm'
)
